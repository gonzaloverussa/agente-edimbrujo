package agent;

import edimbrujologic.Projectile;
import edimbrujologic.Player;
import edimbrujologic.Manager;
import edimbrujologic.Match;
import edimbrujologic.Spawn;
import edimbrujologic.Tower;
import edimbrujologic.Map;
import conexion.Conexion;
import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;

public class exampleAgent {

    public static void main(String[] args) throws IOException {
        Manager manager;
        Player myAgent;
        Map map;
        Match match;
        ArrayList<Player> players;
        ArrayList<Projectile> projectiles;
        ArrayList<Spawn> spawns;
        ArrayList<Tower> towers;
        String myID;
        //Conexion conexion = new Conexion("http://10.0.20.132:8080/Edimbrujo/webservice/server");
        Conexion conexion = new Conexion("http://10.0.20.139:8080/Edimbrujo/webservice/server");
        //Conexion conexion = new Conexion("http://localhost:8080/Edimbrujo/webservice/server");
        manager = Manager.getManager();
        System.out.println(manager);
        myID = conexion.iniciar("gonza");
        manager.updateStaticState(conexion.getFullStaticState());
        while (true) {
            manager.updateState(conexion.getFullState());
            match = manager.getMatch();
            if (match.isStartGame()) {
                myAgent = manager.getPlayer(myID);
                map = manager.getMap();
                players = manager.getPlayers();
                projectiles = manager.getProjectiles();
                spawns = manager.getSpawns();
                towers = manager.getTowers();
                //hacer lo que quieran hacer
                System.out.println(myAgent.getPosition().toString());
                conexion.makeAction("right");
            } else {
                conexion.makeAction("ready");
            }

        }
    }
}
