package agent;

import edimbrujologic.Manager;
import edimbrujologic.Tower;
import conexion.Conexion;
import edimbrujologic.Map;
import edimbrujologic.Match;
import edimbrujologic.Player;
import edimbrujologic.Projectile;
import edimbrujologic.Spawn;
import java.awt.Point;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.LinkedList;

public class EdimbrujoAgent {

    private Manager manager;
    private Tower torreDerecha;
    private Tower torreIzquierda;
    private Tower torreSuperior;

    private Player myAgent;
    private Map map;
    private Match match;

    private ArrayList<Player> players;
    private ArrayList<Projectile> projectiles;
    private ArrayList<Spawn> spawns;
    private ArrayList<Tower> towers;

    private String myID;
    private Conexion con;
    private Movimiento mov;

    private LinkedList<String> listaMov;

    public EdimbrujoAgent() throws IOException {
        con = new Conexion("http://10.0.20.132:8080/Edimbrujo/webservice/server");
        this.myID = con.iniciar("walace47");
        this.manager = Manager.getManager();
        listaMov = new LinkedList<>();
    }

    public void atack() throws IOException {
        cargarTorres();
        listaMov.clear();
        int cuadranteAgente = mov.enQueCuadranteEsta(myAgent.getPosition());
        if (!torreDerecha.isDead()) {
            switch (cuadranteAgente) {
                case (0):
                    mov.inferiorADerecho(myAgent.getPosition(), listaMov);
                    mover();
                    break;
                case (1):
                    hitAndMove(torreDerecha);
                    break;
                default :
                    makeRandomAction();
                    break;

            }
        } else if (!torreIzquierda.isDead()) {
            switch (cuadranteAgente) {
                case (0):
                    System.out.println("cuadrante 0");
                    mov.inferiorAIzquierdo(listaMov, myAgent.getPosition());
                    mover();
                    break;
                case (1):
                    System.out.println("cuadrante 1");
                    mov.derechoAInferior(listaMov, myAgent.getPosition());
                    for (String movement : listaMov) {
                        System.out.println(movement);
                    }
                    mover();
                    break;
                case (2):
                    System.out.println("cuadrante 2");
                    hitAndMove(torreIzquierda);
                    break;
                default:
                    makeRandomAction();
                    break;

            }

        } else if (!torreSuperior.isDead()) {
            switch (cuadranteAgente) {
                case (0):
                    System.out.println("cuadrante 0");
                    mov.inferiorAIzquierdo(listaMov, myAgent.getPosition());
                    mover();
                    break;
                case (1):
                    System.out.println("cuadrante 1");
                    mov.derechoAInferior(listaMov, myAgent.getPosition());
                    mover();
                    break;
                case (2):
                    listaMov.addLast("up");
                    listaMov.addLast("upleft");
                    listaMov.addLast("upright");
                                        listaMov.addLast("up");
                    listaMov.addLast("upleft");
                    listaMov.addLast("upright");
                                        listaMov.addLast("up");
                    listaMov.addLast("upleft");
                    listaMov.addLast("upright");
                                        listaMov.addLast("up");
                    listaMov.addLast("upleft");
                    listaMov.addLast("upright");
                    mover();
                                        makeRandomAction();

                case (4):
                    hitAndMove(torreSuperior);
                    break;
                default:
                    makeRandomAction();
                    break;
            }

        }else{
            makeRandomAction();
        }
    }

    public void makeRandomAction() throws IOException {
        String[] move = {"up", "down", "left", "right", "upleft", "upright", "downleft", "downright"};

        manager.updateState(con.getFullState());
        SecureRandom random = new SecureRandom();
        int i = random.nextInt(1000000000) % 2;
        switch (i) {
            case (0):
                con.makeAction(move[random.nextInt(1000000000) % 8]);
                break;
            case (1):
                con.makeRangeAtack("" + random.nextInt(10000) % 40, "" + random.nextInt(10000) % 40);
                break;

        }

    }

    public void attack() throws IOException {
        manager.updateState(con.getFullState());
        players = manager.getPlayers();
        projectiles = manager.getProjectiles();
        towers = manager.getTowers();
        Tower closestTower = getClosestTower();
        if (myAgent.getPosition().distance(closestTower.getPosition()) > 4) {
            moveCloser(closestTower.getPosition());
        } else {
            hit(closestTower.getPosition());
        }

    }

    private Tower getClosestTower() {
        Tower closestTower = towers.get(0);
        for (Tower tower : towers) {
            if (!tower.isDead() && distanceToPlayer(tower.getPosition())
                    < distanceToPlayer(closestTower.getPosition())) {
                closestTower = tower;
            }
        }
        return closestTower;
    }

    private void moveCloser(Point pointToCloser) throws IOException {
        Point pointToMove = myAgent.getPosition(), pointAgent = myAgent.getPosition();
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                if (x != 0 || y != 0) {
                    Point point = new Point(pointAgent.x + x, pointAgent.y + y);
                    if (canWalk(point)) {
                        if (pointToCloser.distance(point) < pointToCloser.distance(pointToMove)) {
                            pointToMove = point;
                        } else {
                            if (pointToMove.equals(pointAgent)) {
                                pointToMove = point;
                            }
                        }
                    }
                }
            }
        }
        /*if (pointAgent.equals(pointToMove)) {
            while ()
        }*/
        con.makeAction(getMove(pointToMove));
    }

    private double distanceToPlayer(Point point) {
        return myAgent.getPosition().distance(point);
    }

    public void hitAndMove(Tower torre) throws IOException {
        int cuadranteTorre = mov.enQueCuadranteEsta(torre.getPosition());
        do {
            hit(torre.getPosition());
            manager.updateState(con.getFullState());
            agregarMovientoAleatorio(cuadranteTorre);
        } while (!myAgent.isDead());
    }

    private String getMove(Point pointToMove) {
        System.out.println("Mover de " + myAgent.getPosition().toString() + " a " + pointToMove.toString());
        String move = "";
        if (myAgent.getPosition().y < pointToMove.y) {
            move = "down";
        }
        if (myAgent.getPosition().y > pointToMove.y) {
            move = "up";
        }
        if (myAgent.getPosition().x < pointToMove.x) {
            move += "right";
        }
        if (myAgent.getPosition().x > pointToMove.x) {
            move += "left";
        }
        return move;
    }

    public boolean canWalk(Point point) {
        Boolean canWalk = map.canWalk(point);
        if (canWalk) {
            for (Player player : players) {
                if (player.getPosition().equals(point)) {
                    canWalk = false;
                }
            }
            if (canWalk) {
                for (Tower tower : towers) {
                    if (point.equals(tower.getPosition())
                            || point.equals(new Point(tower.getPosition().x + 1, tower.getPosition().y + 1))
                            || point.equals(new Point(tower.getPosition().x + 1, tower.getPosition().y))
                            || point.equals(new Point(tower.getPosition().x + 1, tower.getPosition().y - 1))
                            || point.equals(new Point(tower.getPosition().x, tower.getPosition().y + 1))
                            || point.equals(new Point(tower.getPosition().x, tower.getPosition().y - 1))
                            || point.equals(new Point(tower.getPosition().x - 1, tower.getPosition().y + 1))
                            || point.equals(new Point(tower.getPosition().x - 1, tower.getPosition().y))
                            || point.equals(new Point(tower.getPosition().x - 1, tower.getPosition().y - 1))) {
                        canWalk = false;
                    }
                }
            }
        }
        return canWalk;
    }

    private void agregarMovientoAleatorio(int cuadrante) throws IOException {
        String[] move = {"up", "down", "left", "right", "upleft", "upright", "downleft", "downright"};
        boolean movientoPermitido = false;
        Point posicionAgenteNueva = (Point) myAgent.getPosition().clone();
        do {
            SecureRandom random = new SecureRandom();
            int i = random.nextInt(8);
            switch (i) {
                case (0):
                    posicionAgenteNueva.y--;
                    break;
                case (1):
                    posicionAgenteNueva.y++;
                    break;
                case (2):
                    posicionAgenteNueva.x--;
                    break;
                case (3):
                    posicionAgenteNueva.x++;
                    break;
                case (4):
                    posicionAgenteNueva.y--;
                    posicionAgenteNueva.x--;
                    break;
                case (5):
                    posicionAgenteNueva.y--;
                    posicionAgenteNueva.x++;
                    break;
                case (6):
                    posicionAgenteNueva.y++;
                    posicionAgenteNueva.x--;
                    break;
                case (7):
                    posicionAgenteNueva.y++;
                    posicionAgenteNueva.x++;
                    break;

            }
            if (mov.enQueCuadranteEsta(posicionAgenteNueva) == cuadrante) {
                listaMov.addFirst(move[i]);
                mover();
                movientoPermitido = true;
            }

        } while (!myAgent.isDead() && !movientoPermitido);
        listaMov.clear();

    }

    private void hit(Point punto) throws IOException {
        con.makeRangeAtack("" + punto.x, "" + punto.y);
    }

    public void mover() throws IOException {
        int x, y;
        boolean panic = false;
        for (String list : listaMov) {
            int i = 0;
            do {
                i++;
                x = myAgent.getPosition().x;
                y = myAgent.getPosition().y;
                System.out.println("ejecuto " + list);
                con.makeAction(list);
                manager.updateState(con.getFullState());
                if (i > 3) {
                    String[] move = {"up", "down", "left", "right", "upleft", "upright", "downleft", "downright"};
                    SecureRandom random = new SecureRandom();
                    con.makeAction(move[random.nextInt(8)]);
                    manager.updateState(con.getFullState());
                    panic = true;
                }
            } while (x == myAgent.getPosition().x
                    && y == myAgent.getPosition().y && !panic);
        }
        listaMov.clear();
    }

    public void cargarMapa() throws IOException {
        manager.updateStaticState(con.getFullStaticState());
        map = manager.getMap();
        mov = new Movimiento(map);
        cargarTorres();
    }

    private void cargarTorres() throws IOException {
        manager.updateState(con.getFullState());
        for (int i = 0; i < manager.getTowers().size(); i++) {
            int cuadrante = mov.enQueCuadranteEsta(manager.getTowers().get(i).getPosition().getLocation());
            if (cuadrante == 4) {
                torreSuperior = manager.getTowers().get(i);
                mov.setTorreSuperior(torreSuperior);
             //   System.out.println("Encontre superior");
            } else if (cuadrante == 1) {
                torreDerecha = manager.getTowers().get(i);
                mov.setTorreDerecha(torreDerecha);
                System.out.println("Encontre derecha");
            } else if (cuadrante == 2) {
                torreIzquierda = manager.getTowers().get(i);
                mov.setTorreIzquierda(torreIzquierda);

               // System.out.println("Encontre izquierda");
            }

        }

    }

    public void esperarReady() throws IOException {
        do {
            con.makeAction("ready");
            manager.updateState(con.getFullState());
        } while (!manager.getMatch().isStartGame());
    }

    public void cargarJugador() {
        myAgent = manager.getPlayer(myID);

    }

    public void actuar() throws IOException {
        manager.updateState(con.getFullState());
        if (listaMov.isEmpty()) {
            if (myAgent != null) {
               // System.out.println("(" + myAgent.getPosition().x + "," + myAgent.getPosition().y + ")");
                //System.out.println(mov.enQueCuadranteEsta(myAgent.getPosition()));
                switch (mov.enQueCuadranteEsta(myAgent.getPosition())) {
                    case 0:
                        mov.inferiorADerecho(myAgent.getPosition(), listaMov);
                        break;
                }
            } else {
                cargarJugador();
            }
        } else {
            con.makeAction(listaMov.pop());
        }

    }

    private int calcularDistancia(Point point1, Point point2) {
        double x1, x2, y1, y2;
        x1 = point1.getX();
        y1 = point1.getY();
        x2 = point2.getX();
        y2 = point2.getY();
        return (int) Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
    }
}
