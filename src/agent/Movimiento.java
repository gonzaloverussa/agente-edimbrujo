/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agent;

import edimbrujologic.Map;
import edimbrujologic.Tower;
import java.awt.Point;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author joan
 */
public class Movimiento {

    private Cuadrante cuadranteInferior;// cuadrante 0
    private Cuadrante cuadranteSuperior;// cuadrante 4
    private Cuadrante cuadranteDerecho;// cuadrante 1
    private Cuadrante cuadranteIzquierdo;// cuadrante 2
    private Map mapa;
    private Tower torreDerecha;
    private Tower torreIzquierda;
    private Tower torreSuperior;

    public Movimiento(Map mapa) {
        cargarCuadranteInferior();
        cargarCuadranteDerecho();
        cargarCuadranteSuperior();
        cargarCuadranteIzquierdo();
        this.mapa = mapa;
    }

    private void cargarCuadranteInferior() {
        int xMayor = 28;
        int xMenor = 13;
        int yMenor = 33;
        int yMayor = 37;
        cuadranteInferior = new Cuadrante(
                new Point(xMayor, yMenor),
                new Point(xMenor, yMenor),
                new Point(xMayor, yMayor),
                new Point(xMenor, yMayor));
    }

    private void cargarCuadranteSuperior() {
        int xMayor = 25;
        int xMenor = 14;
        int yMenor = 2;
        int yMayor = 24;
        cuadranteSuperior = new Cuadrante(
                new Point(xMayor, yMenor),
                new Point(xMenor, yMenor),
                new Point(xMayor, yMayor),
                new Point(xMenor, yMayor));
    }

    private void cargarCuadranteIzquierdo() {
        int xMayor = 16;
        int xMenor = 5;
        int yMenor = 17;
        int yMayor = 27;
        cuadranteIzquierdo = new Cuadrante(
                new Point(xMayor, yMenor),
                new Point(xMenor, yMenor),
                new Point(xMayor, yMayor),
                new Point(xMenor, yMayor));
    }

    private void cargarCuadranteDerecho() {
        int xMayor = 36;
        int xMenor = 23;
        int yMenor = 17;
        int yMayor = 27;
        cuadranteDerecho = new Cuadrante(
                new Point(xMayor, yMenor),
                new Point(xMenor, yMenor),
                new Point(xMayor, yMayor),
                new Point(xMenor, yMayor));
    }

    /*public int enQueCuadranteEsta(Point punto) {
        int res = 99;
        //0 inferior
        //1 derecho
        //2 izquierdo
        //3 superior
        if (punto.y >= cuadranteInferior.getSuperiorIzquiedo().y
                && punto.y <= cuadranteInferior.getInferiorIzquiedo().y) {
            res = 0;
        } else if (punto.y >= cuadranteSuperior.getSuperiorIzquiedo().y
                && punto.y <= cuadranteSuperior.getInferiorIzquiedo().y) {
            res = 4;
        } else if (punto.x >= cuadranteDerecho.getInferiorIzquiedo().x
                && punto.x <= cuadranteDerecho.getInferiorDerecha().x) {
            res = 1;
        } else if(punto.x >= cuadranteIzquierdo.getInferiorIzquiedo().x
                && punto.x <= cuadranteIzquierdo.getInferiorDerecha().x){
            res = 2;
        }
        return res;

    }*/
    public int enQueCuadranteEsta(Point punto){
        int res= 99;
        if(cuadranteSuperior.elPuntoEstaAdentro(punto)){
            res=4;
        }else if(cuadranteDerecho.elPuntoEstaAdentro(punto)){
            res = 1;
        }else if(cuadranteIzquierdo.elPuntoEstaAdentro(punto)){
            res = 2;
        }else if(cuadranteInferior.elPuntoEstaAdentro(punto)){
            res = 0;
        }
        return res;
    }

    public void inferiorADerecho(Point punto, LinkedList<String> lista) {
        Point puntoReferencia = moverPuntoReferenciaInferior(lista, punto);
        for (int i = puntoReferencia.y; i > 25; i--) {
            lista.addLast("up");
        }
        lista.addLast("up");
        for (String lista1 : lista) {
            System.out.println(lista1);
        }
    }

    public void setTorreDerecha(Tower torreDerecha) {
        this.torreDerecha = torreDerecha;
    }

    public void setTorreIzquierda(Tower torreIzquierda) {
        this.torreIzquierda = torreIzquierda;
    }

    public void setTorreSuperior(Tower torreSuperior) {
        this.torreSuperior = torreSuperior;
    }
    
    

    private Point moverPuntoReferenciaInferior(LinkedList<String> lista, Point punto) {
        int y = punto.y - 35;
        //si el jugado esta en el 34 subo 1
        System.out.println("y = " + y);
        if (y < 0) {
            lista.addLast("up");
            y = 35;
        } else {
            y = punto.y;
        }
        //Luego me muevo hasta el x = 28
        int x = punto.x - 28;
        System.out.println("x = " + x);
        if (x < 0) {
            for (int i = x; i <= 0; i++) {
                lista.addLast("right");
            }
        }
        x = 28;
        return new Point(x, y);
    }

    public void derechoAInferior(LinkedList<String> lista, Point punto) {
        Point puntoReferencia = moverPuntoReferenciaDerechoAInferior(lista, punto);
        int y = 34 - puntoReferencia.y;
        for (int i = y; i > 0; i--) {
            lista.addLast("down");
        }
    }

    private Point moverPuntoReferenciaDerechoAInferior(LinkedList<String> lista, Point punto) {
        int x = 28;
        int y = punto.y;
        Point puntoTorre = torreDerecha.getPosition();
        if (punto.x > puntoTorre.x + 1 && (punto.y >= puntoTorre.y - 1 && punto.y <= puntoTorre.y + 1)) {
            lista.addLast("down");
            lista.addLast("down");
            lista.addLast("down");
            y = y - 2;
        }
        int i = x - punto.x;
        if (i > 0) {
            for (int j = i; j > 0; j--) {
                lista.addLast("rigth");
            }
        } else {
            for (int j = i; j < 0; j++) {
                lista.addLast("left");
            }
        }
        return new Point(x, y);
    }

    public void inferiorAIzquierdo(LinkedList<String> lista, Point punto) {
        Point puntoReferencia = moverPuntoReferenciaInferiorAIzquierdo(lista, punto);
        for (int i = puntoReferencia.y - 25; i >=0; i--){
            lista.addLast("up");
        }
    }
    
     private Point moverPuntoReferenciaInferiorAIzquierdo(LinkedList<String> lista, Point punto) {
       int x = 13;
       for (int i = punto.x - x; i >=0; i--){
           lista.addLast("left");
       }
       return new Point(x,punto.y);
    }
     
    public void izquierdoASuperior(LinkedList<String> lista, Point punto) {
        Point puntoReferencia = moverPuntoReferenciaInferiorAIzquierdo(lista, punto);
        for (int i = puntoReferencia.y - 25; i >=0; i--){
            lista.addLast("up");
        }
    }
    

}
