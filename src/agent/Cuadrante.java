/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agent;

import java.awt.Point;

/**
 *
 * @author joan
 */
public class Cuadrante {
    private  Point superiorDerecha;
    private  Point superiorIzquiedo;
    private  Point inferiorDerecha;
    private  Point inferiorIzquiedo;

    Cuadrante() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Point getSuperiorDerecha() {
        return superiorDerecha;
    }

    public Point getSuperiorIzquiedo() {
        return superiorIzquiedo;
    }

    public Point getInferiorDerecha() {
        return inferiorDerecha;
    }

    public Point getInferiorIzquiedo() {
        return inferiorIzquiedo;
    }

    public Cuadrante(Point superiorDerecha, Point superiorIzquiedo, Point inferiorDerecha, Point inferiorIzquiedo) {
        this.superiorDerecha = superiorDerecha;
        this.superiorIzquiedo = superiorIzquiedo;
        this.inferiorDerecha = inferiorDerecha;
        this.inferiorIzquiedo = inferiorIzquiedo;
    }
    
    public boolean elPuntoEstaAdentro(Point punto){
        return punto.x >=  inferiorIzquiedo.x &&
                punto.x<= inferiorDerecha.x &&
                punto.y >= superiorIzquiedo.y &&
                punto.y <= inferiorIzquiedo.y;
    }
   
}
